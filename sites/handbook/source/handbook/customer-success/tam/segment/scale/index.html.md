---
layout: handbook-page-toc
title: "TAM Segment: Scale"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

⚠️ This information is under active development and will continue to evolve. This page will be updated on an ongoing basis with our roll-out iterations.
{: .alert .alert-warning}

## Overview

Definition: The Scale TAM team is a new team being established at GitLab in FY23, a key tenant in our customer success strategy to align with our [company strategy around establishing and expanding our customer-centric approach and focus](https://about.gitlab.com/company/strategy/#3-customer-centricity). 

Scale TAMs driving programmatic enablement and targeted customer engagement across the longtail customer base. [For the ARR thresholds for the Scale customer segment please reference this Wiki page](https://gitlab.com/gitlab-com/customer-success/tam/-/wikis/TAM-Segments) (GitLab Internal)

## Customer lifecycle

The diagram below outlines the customer lifecycle for our Scale Customers:

![Customer Lifecycle Journey Scale TAM](/images/handbook/customer-success/customer_lifecycle_journey_with_metrics_-_scale_tam_2.png)

## Motions

### Align

This cohort is enabled primarily through shared content and one-to-many webinars. See our [TAM Webinar Calendar](https://about.gitlab.com/handbook/customer-success/tam/segment/scale/webinar-calendar/) for upcoming events. The primary focus for one-to-one interactions will put all first-year customers on a path for success through removing barriers in the first few months of their onboarding and adoption. Our secondary focus is around engaging with specific at-risk customers throughout the customer journey. 

A key metric we will use to systematically identify at-risk customers will be license utilization. For example, customers who haven't reached 75% license utilization and are past the six-month mark in their license will be triggered to Scale TAMs to engage. 

### Enable

Onboarding and enablement for this customer segment are primarily through one-to-many webinar sessions. In addition, a personal TAM reach-out occurs on day 21 to ensure the customer is on-track for success. A Scale TAM's goal with any one-to-one customer interaction is to put the customer on a path for success and ensure they know their ongoing resources (support team, blog, learning resources, etc.) 

A Scale TAM's engagement with any one customer will be limited in total customer interactions, most often to two or three. Additional targeted customer reach-outs for triggered events such as missed time-to-first-value poor onboarding NPS scores may occur throughout the customer lifecycle.

#### Metrics for Enable

Onboarding:

1. Net-new customer attendance in onboarding webinars
1. Time to First Value
1. % 21 day calls completed
1. Onboarding NPS & CSAT scores
1. License utilization

Use-Case Enablement:

1. Customer attendance in enablement webinars
1. Use Case Health scores
1. License utilization percentage 

### Expand & Renew

Expansion is primarily driven by the SAL or AE in this segment, though a key driver for expansion is product enablement and familiarity. Scale TAMs will be looking for expansion opportunities in their conversations with customers, and will pass along potential opportunities to their sales counterparts to do further discovery and demos. Targeted email campaigns based on expansion potential (LAM) may be sent from Scale TAMs periodically to drive expansion as well. 

#### Metrics for Expand & Renew

1. Net and Gross Retention for the customer segment (Renewal and Expansion rate)
1. License utilization
1. Customer attendance in expansion webinars
1. New Use Cases Adopted
1. Renewal NPS & CSAT Scores
1. Number of opportunities passed to sales reps

### Scale TAM Role & Responsibilities 

A core responsiblity for Scale TAMs is engaging with the customer base in targeted one-to-one interactions. As a result, the following expectations for weekly customer engagements have been established for Scale TAMs. These metrics keep in mind the importance of focusing on interacting with customers live, while also allowing time to prepare for and follow up with customers after these interactions:  

- 15 customer calls per week (3 per day)

For an additional overview of our Scale team-specific responsibilities, please reference our Job Family: [Scale TAM Responsibilities & Requirements](https://about.gitlab.com/job-families/sales/technical-account-manager/#scale)

### Additional Resources

Check out the "[How GitLab Scaled Customer Value For Long Tail Customers](https://www.valuize.co/all-resources/gitlab-scale-strategy-webinar/)" webinar with Sherrod Patching, Head of Global TAM (Customer Success) at GitLab, and Ross Fulton, Founder & CEO at Valuize. This discussion is about how GitLab built its scale strategy through a programmatic, data-driven approach and various methods of driving value throughout the customer journey.
